<?php
// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );

remove_action('wp_head', 'wp_generator');
// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );

add_action( 'wp_enqueue_scripts', function(){
wp_enqueue_style("font",get_stylesheet_directory_uri()."/resources/UltimateIcons.css");
    wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
    wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.min.js","","",1);
    wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/script.js","","",1);
});


// Register menus
function register_my_menus() {
    register_nav_menus(
        array(
            'footer-1' => __( 'Footer Menu 1' ),
            'footer-2' => __( 'Footer Menu 2' ),
            'footer-3' => __( 'Footer Menu 3' ),
            'footer-4' => __( 'Footer Menu 4' ),
            'footer-5' => __( 'Footer Menu 5' ),
            'site-map' => __( 'Site Map' ),
        )
    );
}
add_action( 'init', 'register_my_menus' );

// Enable shortcodes in text widgets
add_filter('widget_text','do_shortcode');

// Move Yoast to bottom
function yoasttobottom()
{
    return 'low';
}

add_filter('wpseo_metabox_prio', 'yoasttobottom');

function fr_img($id=0,$size="",$url=false,$attr=""){

    //Show a theme image
    if(!is_numeric($id) && is_string($id)){
        $img=get_stylesheet_directory_uri()."/images/".$id;
        if(file_exists(to_path($img))){
            if($url){
                return $img;
            }
            return '<img src="'.$img.'" '.($attr?build_attr($attr):"").'>';
        }
    }

    //If ID is empty get the current post attachment id
    if(!$id){
        $id=get_post_thumbnail_id();
    }

    //If Id is object it means that is a post object, thus retrive the post ID
    if(is_object($id)){
        if(!empty($id->ID)){
            $id=$id->ID;
        }
    }

    //If ID is not an attachment than get the attachment from that post
    if(get_post_type($id)!="attachment"){
        $id=get_post_thumbnail_id($id);
    }

    if($id){
        $image_url=wp_get_attachment_image_url($id,$size);
        if(!$url){
            //If image is a SVG embed the contents so we can change the color dinamically
            if(substr($image_url,-4,4)==".svg"){
                $image_url=str_replace(get_bloginfo("url"),ABSPATH."/",$image_url);
                $data=file_get_contents($image_url);
                echo strstr($data,"<svg ");
            }else{
                return wp_get_attachment_image($id,$size,0,$attr);
            }
        }else if($url){
            return $image_url;
        }
    }
}

//Facet Title Hook
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
    if ( isset( $atts['facet'] ) ) {       
        $output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
    }
    return $output;
}, 10, 2 );

add_filter('wp_nav_menu_items', 'do_shortcode');

/**
 * Dequeue the jQuery UI styles.
 *
 * Hooked to the wp_print_styles action, with a late priority (100),
 * so that it is after the style was enqueued.
 */
 function remove_pagelist_css() {
    wp_dequeue_style( 'page-list-style' );
 }
 add_action( 'wp_print_styles', 'remove_pagelist_css', 100 );


 
// IP location FUnctionality
if (!wp_next_scheduled('cde_preferred_location_cronjob')) {
    
    wp_schedule_event( time() +  17800, 'daily', 'cde_preferred_location_cronjob');
}

//add_action( 'cde_preferred_location_cronjob', 'cde_preferred_location' );

function cde_preferred_location(){

          global $wpdb;

          if ( ! function_exists( 'post_exists' ) ) {
              require_once( ABSPATH . 'wp-admin/includes/post.php' );
          }

          //CALL Authentication API:
          $apiObj = new APICaller;
          $inputs = array('grant_type'=>'client_credentials','client_id'=>get_option('CLIENT_CODE'),'client_secret'=>get_option('CLIENTSECRET'));
          $result = $apiObj->call(AUTHURL,"POST",$inputs,array(),AUTH_BASE_URL);


          if(isset($result['error'])){
              $msg =$result['error'];                
              $_SESSION['error'] = $msg;
              $_SESSION["error_desc"] =$result['error_description'];
              
          }
          else if(isset($result['access_token'])){

              //API Call for getting website INFO
              $inputs = array();
              $headers = array('authorization'=>"bearer ".$result['access_token']);
              $website = $apiObj->call(BASEURL.get_option('SITE_CODE'),"GET",$inputs,$headers);

             // write_log($website['result']['locations']);

              for($i=0;$i<count($website['result']['locations']);$i++){

                  if($website['result']['locations'][$i]['type'] == 'store'){

                      $location_name = isset($website['result']['locations'][$i]['city'])?$website['result']['locations'][$i]['name']:"";

                      $found_post = post_exists($location_name,'','','store-locations');

                          if( $found_post == 0 ){

                                  $array = array(
                                      'post_title' => $location_name,
                                      'post_type' => 'store-locations',
                                      'post_content'  => "",
                                      'post_status'   => 'publish',
                                      'post_author'   => 0,
                                  );
                                  $post_id = wp_insert_post( $array );

                                //  write_log( $location_name.'---'.$post_id);
                                  
                                  update_post_meta($post_id, 'address', $website['result']['locations'][$i]['address']); 
                                  update_post_meta($post_id, 'city', $website['result']['locations'][$i]['city']); 
                                  update_post_meta($post_id, 'state', $website['result']['locations'][$i]['state']); 
                                  update_post_meta($post_id, 'country', $website['result']['locations'][$i]['country']); 
                                  update_post_meta($post_id, 'postal_code', $website['result']['locations'][$i]['postalCode']); 
                                  if($website['result']['locations'][$i]['forwardingPhone']==''){

                                    update_post_meta($post_id, 'phone', $website['result']['locations'][$i]['phone']);  
                                    
                                  }else{

                                    update_post_meta($post_id, 'phone', $website['result']['locations'][$i]['forwardingPhone']);  
                                  }
                                                                     
                                  update_post_meta($post_id, 'latitude', $website['result']['locations'][$i]['lat']); 
                                  update_post_meta($post_id, 'longitue', $website['result']['locations'][$i]['lng']); 
                                  update_post_meta($post_id, 'monday', $website['result']['locations'][$i]['monday']); 
                                  update_post_meta($post_id, 'tuesday', $website['result']['locations'][$i]['tuesday']); 
                                  update_post_meta($post_id, 'wednesday', $website['result']['locations'][$i]['wednesday']); 
                                  update_post_meta($post_id, 'thursday', $website['result']['locations'][$i]['thursday']); 
                                  update_post_meta($post_id, 'friday', $website['result']['locations'][$i]['friday']); 
                                  update_post_meta($post_id, 'saturday', $website['result']['locations'][$i]['saturday']); 
                                  update_post_meta($post_id, 'sunday', $website['result']['locations'][$i]['sunday']); 
                                  
                          }else{

                                    update_post_meta($found_post, 'address', $website['result']['locations'][$i]['address']); 
                                    update_post_meta($found_post, 'city', $website['result']['locations'][$i]['city']); 
                                    update_post_meta($found_post, 'state', $website['result']['locations'][$i]['state']); 
                                    update_post_meta($found_post, 'country', $website['result']['locations'][$i]['country']); 
                                    update_post_meta($found_post, 'postal_code', $website['result']['locations'][$i]['postalCode']); 
                                    if($website['result']['locations'][$i]['forwardingPhone']==''){

                                    update_post_meta($found_post, 'phone', $website['result']['locations'][$i]['phone']);  
                                    
                                    }else{

                                    update_post_meta($found_post, 'phone', $website['result']['locations'][$i]['forwardingPhone']);  
                                    }
                                                                    
                                    update_post_meta($found_post, 'latitude', $website['result']['locations'][$i]['lat']); 
                                    update_post_meta($found_post, 'longitue', $website['result']['locations'][$i]['lng']); 
                                    update_post_meta($found_post, 'monday', $website['result']['locations'][$i]['monday']); 
                                    update_post_meta($found_post, 'tuesday', $website['result']['locations'][$i]['tuesday']); 
                                    update_post_meta($found_post, 'wednesday', $website['result']['locations'][$i]['wednesday']); 
                                    update_post_meta($found_post, 'thursday', $website['result']['locations'][$i]['thursday']); 
                                    update_post_meta($found_post, 'friday', $website['result']['locations'][$i]['friday']); 
                                    update_post_meta($found_post, 'saturday', $website['result']['locations'][$i]['saturday']); 
                                    update_post_meta($found_post, 'sunday', $website['result']['locations'][$i]['sunday']); 

                          }

                  }
              
              }

          }    

}

 function getUserIpAddr() {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
	
	if ( strstr($ipaddress, ',') ) {
            $tmp = explode(',', $ipaddress,2);
            $ipaddress = trim($tmp[1]);
    }
    return $ipaddress;
}

add_action( 'wp_ajax_nopriv_choose_location', 'choose_location' );
add_action( 'wp_ajax_choose_location', 'choose_location' );

function choose_location() {     

    global $wpdb;
      
    $postid = $wpdb->get_var( "SELECT ID FROM $wpdb->posts WHERE post_title LIKE '%".$_POST['store_id']."%' AND post_type = 'store-locations'" );

    $data = array();

    $data['header_location_name']= get_the_title($postid);
    $data['header_short_name'] = get_field('store_short_name', $postid);
    $data['header_phone'] = formatPhoneNumber(get_field('phone', $postid));
    $data['header_address'] = get_field('address', $postid);
    $data['header_city'] = get_field('city', $postid);
    $data['header_state'] = get_field('state', $postid);
    $data['header_zipcode'] = get_field('postal_code', $postid);
    $data['header_store_url'] = get_field('store_site_url', $postid);
    
    
    $data['store_id'] = $postid;
    echo json_encode($data);

      wp_die();
}

//add_shortcode('store_distance', 'distance_function');

add_action( 'wp_ajax_nopriv_distance_function', 'distance_function', '1' );
add_action( 'wp_ajax_distance_function', 'distance_function','1' );
function distance_function( $atts = array() ) {

    $data = array();
    global $wpdb;
    $urllog = 'https://global-ds.cloud.netacuity.com/webservice/query';
  
  $response = wp_remote_post( $urllog, array(
      'method' => 'GET',
      'timeout' => 45,
      'redirection' => 5,
      'httpversion' => '1.0',
      'headers' => array('Content-Type' => 'application/json'),         
      'body' =>  array('u'=> 'b25d7667-74cc-4fcc-9adf-7b5f4f8f5bd0','ip'=> getUserIpAddr(),'dbs'=> 'all','trans_id'=> 'example','json'=> 'true' ),
      'blocking' => true,               
      'cookies' => array()
      )
  );
      
      $rawdata = json_decode($response['body'], true);
      $userdata = $rawdata['response'];

      $autolat = $userdata['pulseplus-latitude'];
      $autolng = $userdata['pulseplus-longitude'];

      $sql =  "SELECT post_lat.meta_value AS lat,post_lng.meta_value AS lng,posts.ID, 
              ( 3959 * acos( cos( radians(".$autolat." ) ) * cos( radians( post_lat.meta_value ) ) * 
              cos( radians( post_lng.meta_value ) - radians( ".$autolng." ) ) + sin( radians( ".$autolat." ) ) * 
              sin( radians( post_lat.meta_value ) ) ) ) AS distance FROM wp_posts AS posts
              INNER JOIN wp_postmeta AS post_lat ON post_lat.post_id = posts.ID AND post_lat.meta_key = 'latitude'
              INNER JOIN wp_postmeta AS post_lng ON post_lng.post_id = posts.ID AND post_lng.meta_key = 'longitue'  
              WHERE posts.post_type = 'store-locations' 
              AND posts.post_status = 'publish' GROUP BY posts.ID HAVING distance < 50000000000000000000 ORDER BY distance";

      $storeposts = $wpdb->get_results($sql);

      //write_log($storeposts);

      $storeposts_array = json_decode(json_encode($storeposts), true);    

            foreach($storeposts as $store){

                 $store_title =  get_the_title($store->ID);

                if (strpos($store_title, 'Arlington Heights') !== false) {

                $data['arlington_distance'] = round($store->distance,1);

                }
                if (strpos($store_title, 'Waukesha') !== false) {

                $data['waukesha_distance'] = round($store->distance,1);

                } 
                if (strpos($store_title, 'Madison') !== false) {

                    $data['madison_distance'] = round($store->distance,1);
                }

            }  
    
            echo json_encode($data);

            wp_die();
}

// Custom function for lat and long

function get_storelisting() {
  
    global $wpdb;
    $content="";
  
  
  //  echo 'no cookie';
    
    $urllog = 'https://global-ds.cloud.netacuity.com/webservice/query';
    //12.68.65.195
    $response = wp_remote_post( $urllog, array(
        'method' => 'GET',
        'timeout' => 45,
        'redirection' => 5,
        'httpversion' => '1.0',
        'headers' => array('Content-Type' => 'application/json'),         
        'body' =>  array('u'=> 'b25d7667-74cc-4fcc-9adf-7b5f4f8f5bd0','ip'=> getUserIpAddr(),'dbs'=> 'all','trans_id'=> 'example','json'=> 'true' ),
        'blocking' => true,               
        'cookies' => array()
        )
    );
        
        $rawdata = json_decode($response['body'], true);
        $userdata = $rawdata['response'];
  
        $autolat = $userdata['pulseplus-latitude'];
        $autolng = $userdata['pulseplus-longitude'];
        
  
        //print_r($rawdata);
        
        $sql =  "SELECT post_lat.meta_value AS lat,post_lng.meta_value AS lng,posts.ID, 
                ( 3959 * acos( cos( radians(".$autolat." ) ) * cos( radians( post_lat.meta_value ) ) * 
                cos( radians( post_lng.meta_value ) - radians( ".$autolng." ) ) + sin( radians( ".$autolat." ) ) * 
                sin( radians( post_lat.meta_value ) ) ) ) AS distance FROM wp_posts AS posts
                INNER JOIN wp_postmeta AS post_lat ON post_lat.post_id = posts.ID AND post_lat.meta_key = 'latitude'
                INNER JOIN wp_postmeta AS post_lng ON post_lng.post_id = posts.ID AND post_lng.meta_key = 'longitue'  
                WHERE posts.post_type = 'store-locations' 
                AND posts.post_status = 'publish' GROUP BY posts.ID HAVING distance < 50000000000000000000 ORDER BY distance";
  
              //  write_log($sql);
  
        $storeposts = $wpdb->get_results($sql);
        $storeposts_array = json_decode(json_encode($storeposts), true);      
  
        if(isset($_COOKIE['preferred_store'])){
  
            $store_location = $_COOKIE['preferred_store'];

        }else{
  
            $store_location = $storeposts_array['0']['ID'];
           
          }
          if(isset($_COOKIE['preferred_distance'])){
  
            $store_distance = $_COOKIE['preferred_distance'];

        }else{
  
            $store_distance = $storeposts_array['0']['distance'];
           
          }
        // if($store_location ==''){
        //     //write_log('empty loc');           
        //     $store_location = $storeposts_array['0']['ID'];
        //     $store_distance = round($storeposts_array['0']['distance'],1);
        //     $city = get_field('city',$store_location);

           
        // }else{
  
        // //   write_log('noempty loc');
        //     $key = array_search($store_location, array_column($storeposts_array, 'ID'));
        //   //  write_log($key);
        //     $store_distance = round($storeposts_array[$key]['distance'],1);       
        //     $store_location = $storeposts_array['0']['ID'];    
  
        // }
        
     //   write_log($store_location);
      //  write_log($_COOKIE['preferred_store']);
      
        $content = '<h5 class="location_name">'.get_the_title($store_location).'</h5>
        
        <span class="loc_phone"><a href="tel:'.get_field('phone', $store_location).'"><i class="fa fa-phone-alt"></i> &nbsp;'.get_field('phone', $store_location).'</a></span>';     
        

        $content_list = "";
    foreach ( $storeposts as $post ) {
  
        $cityname = explode(",",get_field('city',$post->ID));
                 
                  $content_list .= '<div class="location-section" id ="'.$post->ID.'">
                  <div class="location-info-section">
                      <h2 class="location-name">'.get_the_title($post->ID).' - <b>'.round($post->distance,1).' MI</b></h2>
                      <p class="store-add"> '.get_field('address', $post->ID).'<br />'.get_field('city',$post->ID).', '.get_field('state',$post->ID).' '.get_field('postal_code', $post->ID).'</p>
                      <p class="store-phone"><a href="tel:'.get_field('phone', $post->ID).'"><i class="fa fa-phone-alt"></i> &nbsp;'.get_field('phone', $post->ID).'</a></p>
                      <div class="store-hour-section">
                          <h5 class="label">STORE HOURS</h5>
                          <ul class="storename"><li><div class="store-opening-hrs-container">
                          <ul class="store-opening-hrs">
                              <li>Monday: <span>'.get_field('monday', $post->ID).'</span></li>
                              <li>Tuesday: <span>'.get_field('tuesday', $post->ID).'</span></li>
                              <li>Wednesday: <span>'.get_field('wednesday', $post->ID).'</span></li>
                              <li>Thursday: <span>'.get_field('thursday', $post->ID).'</span></li>
                              <li>Friday: <span>'.get_field('friday', $post->ID).'</span></li>
                              <li>Saturday: <span>'.get_field('saturday', $post->ID).'</span></li>
                              <li>Sunday: <span>'.get_field('sunday', $post->ID).'</span></li>
                          </ul>
                          </div></li></ul>
                      </div>
                      '.do_shortcode('[storelocation_address "dir" "'.get_the_title($post->ID).'"]').'
                      <a class="links" href="/request-appointment/">REQUEST APPOINTMENT</a>
                      
                      <a href="javascript:void(0)" id="'.$cityname[0].'" data-state="'.get_field('state',$post->ID).'" data-id="'.$post->ID.'" data-storename="'.get_the_title($post->ID).'" data-phone="'.get_field('phone', $post->ID).'" data-distance="'.round($post->distance,1).'" target="_self" class="store-cta-link choose_location">Choose This Location</a>
                  </div>
              </div>'; 
    } 
    
  
    $data = array();
  
    $data['header'] = $content;
    $data['list'] = $content_list;
    $data['store_location'] = get_the_title($store_location);
    $data['distance'] = $store_distance;
    $data['store_name'] = get_field('store_short_name', $store_location); 
    $data['address'] =  get_field('address', $store_location);
    $data['state'] =  get_field('state', $store_location);
    $data['city'] =  get_field('city', $store_location);
    $data['postal_code']=  get_field('postal_code', $store_location);
    $data['store_url']=  get_field('store_site_url', $store_location);
    $data['store_id']=  $store_location;

   // write_log($data['list']);
  
    echo json_encode($data);
        wp_die();
  }
  
  //add_shortcode('storelisting', 'get_storelisting');
  add_action( 'wp_ajax_nopriv_get_storelisting', 'get_storelisting', '1' );
  add_action( 'wp_ajax_get_storelisting', 'get_storelisting','1' );
  

add_shortcode('closest_storemap', 'closeststoremap_function');
function closeststoremap_function() {

    global $wpdb;
   
    $urllog = 'https://global-ds.cloud.netacuity.com/webservice/query';    
         
            $response = wp_remote_post( $urllog, array(
                'method' => 'GET',
                'timeout' => 45,
                'redirection' => 5,
                'httpversion' => '1.0',
                'headers' => array('Content-Type' => 'application/json'),         
                'body' =>  array('u'=> 'b25d7667-74cc-4fcc-9adf-7b5f4f8f5bd0','ip'=> getUserIpAddr(),'dbs'=> 'all','trans_id'=> 'example','json'=> 'true' ),
                'blocking' => true,               
                'cookies' => array()
                )
            );

            $rawdata = json_decode($response['body'], true);
            $userdata = $rawdata['response'];
      
            $autolat = $userdata['pulseplus-latitude'];
            $autolng = $userdata['pulseplus-longitude'];
            

            $sql =  "SELECT post_lat.meta_value AS lat,post_lng.meta_value AS lng,posts.ID, 
                        ( 3959 * acos( cos( radians(".$autolat." ) ) * cos( radians( post_lat.meta_value ) ) * 
                        cos( radians( post_lng.meta_value ) - radians( ".$autolng." ) ) + sin( radians( ".$autolat." ) ) * 
                        sin( radians( post_lat.meta_value ) ) ) ) AS distance FROM wp_posts AS posts
                        INNER JOIN wp_postmeta AS post_lat ON post_lat.post_id = posts.ID AND post_lat.meta_key = 'latitude'
                        INNER JOIN wp_postmeta AS post_lng ON post_lng.post_id = posts.ID AND post_lng.meta_key = 'longitue'  
                        WHERE posts.post_type = 'store-locations' 
                        AND posts.post_status = 'publish' GROUP BY posts.ID HAVING distance < 50000000000000000000 ORDER BY distance";
        
                    
        
                $storeposts = $wpdb->get_results($sql);
                $storeposts_array = json_decode(json_encode($storeposts), true);    

                $store_location = $storeposts_array['0']['ID'];
       
   
         $store_name  = get_the_title( $store_location );
     

         $store_address = get_field('store_short_name', $store_location).' '.get_field('address', $store_location).' '.get_field('city',$store_location).', '.get_field('state',$store_location).', '.get_field('postal_code', $store_location);
     
         return $store_address;
     

}


/*:::::::::::::::::::::::Workbook code start heree:::::::::::::::::::::::::::: */

add_action( 'wp_ajax_nopriv_add_fav_product', 'add_fav_product' );
add_action( 'wp_ajax_add_fav_product', 'add_fav_product' );

function add_fav_product() {
    $is_fav = $_POST['is_fav'];
    $post_id = $_POST['post_id'];

    $result = update_post_meta( $post_id, 'is_fav', $is_fav);
if($result == false){
    $result1 = add_post_meta( $post_id, 'is_fav', $is_fav);
}
}

add_action( 'wp_ajax_nopriv_base64_to_jpeg_convert', 'base64_to_jpeg_convert' );
add_action( 'wp_ajax_base64_to_jpeg_convert', 'base64_to_jpeg_convert' );

function base64_to_jpeg_convert() {    
    global $wpdb;

    $upload_dir = wp_get_upload_dir();
    $output_file= $upload_dir['basedir']. '/measure/'.uniqid().'.png';

    $img = $_POST['imagedata']; 
    $img = str_replace('data:image/png;base64,', '', $img);
    $img = str_replace(' ', '+', $img);
    $data = base64_decode($img);
    file_put_contents($output_file, $data);
    
    if ( is_user_logged_in() ) {
        $current_user = wp_get_current_user();
        write_log( 'Personal Message For '. $current_user->ID ) ;
     
        $data=array(
            'userid' => $current_user->ID, 
            'image_name' => $_POST['imagename'],
            'image_path' => $output_file 
        );    
    
         $wpdb->insert( 'wp_measure_images', $data);

         $measureimages = $wpdb->get_results("SELECT * FROM wp_measure_images WHERE userid = $current_user->ID ");

        write_log($measureimages);

        
        $content = '<div id="mesureMentprintMe" class="product-plp-grid product-grid swatch "><div class="row product-row" id="aniimated-thumbnials">';

        $content .= '<h3 style="text-align: center; font-size: 33px;"><span>SAVED MEASUREMENTS</span></h3>';

        foreach($measureimages as $img){

           

            $img_path = str_replace('/var/www/html', '', $img->image_path);

            $image_url = home_url().''.$img_path;

            $content .= '<div class="col-lg-4 col-md-4 col-sm-6 "><div class="fl-post-grid-post">';
            $content .= '<div class="fl-post-grid-image prod_like_wrap mesureMentWrap">';

            $content .= '<a href="javascript:void(0);" title="#" class="wplightbox"><img class="measure_img" src="'.home_url().''.$img_path.'" /></a>';

            $content .= '<div class="favButtons button-wrapper">
                <a class="button fl-button view" href="javascript:void(0);" title="#"  data-img="'.home_url().''.$img_path.'" onclick="openPopUp(this)" data-title="'.$img->image_name.'">VIEW</a>';
                $content .= '<a class="button fl-button deletemeasure" href="javascript:void(0)" data-id="'.$img->id.'"><i class="fa fa-trash" aria-hidden="true"></i></a>'; 
                $content .= '</div> </div>';
                $content .= '<h3><span>'.$img->image_name.'</span></h3>';

                $content .= "</div></div>";

        }

        $content .= "</div></div>";

        echo $content;


        wp_die();
    }

}

function measurement_tool_images($arg){    
    global $wpdb;

    if ( is_user_logged_in() ) {
        $current_user = wp_get_current_user();
        write_log( 'Personal Message For '. $current_user->ID ) ;

        $measureimages = $wpdb->get_results("SELECT * FROM wp_measure_images WHERE userid = $current_user->ID ");

        write_log($measureimages);

        if($measureimages){

        
        $content = '<div id="mesureMentprintMe" class="product-plp-grid product-grid swatch "><div class="row product-row" id="aniimated-thumbnials">';
        $content .= '<h3 style="text-align: center; font-size: 33px;"><span>SAVED MEASUREMENTS</span></h3>';

        foreach($measureimages as $img){

           

            $img_path = str_replace('/var/www/html', '', $img->image_path);

            $image_url = home_url().''.$img_path;

            $content .= '<div class="col-lg-4 col-md-4 col-sm-6 "><div class="fl-post-grid-post">';
            $content .= '<div class="fl-post-grid-image prod_like_wrap mesureMentWrap">';

            $content .= '<a href="javascript:void(0);" title="#" class="wplightbox"><img class="measure_img" src="'.home_url().''.$img_path.'" /></a>';

            $content .= '<div class="favButtons button-wrapper">
                <a class="button fl-button view" href="javascript:void(0);" title="#"  data-img="'.home_url().''.$img_path.'" onclick="openPopUp(this)" data-title="'.$img->image_name.'">VIEW</a>';
                $content .= '<a class="button fl-button deletemeasure" href="#" data-id="'.$img->id.'"><i class="fa fa-trash" aria-hidden="true"></i></a>'; 
                $content .= '</div> </div>';
                $content .= '<h3><span>'.$img->image_name.'</span></h3>';

                $content .= "</div></div>";

        }

        $content .= "</div></div>";
        

        return $content;
      }
    }
   
  
}    
add_shortcode('measurementtool_images', 'measurement_tool_images');


add_action( 'wp_ajax_nopriv_delete_measureimg', 'delete_measurement_images' );
add_action( 'wp_ajax_delete_measureimg', 'delete_measurement_images' );

function delete_measurement_images() {  
    global $wpdb;

    $mid = $_POST['mimg_id'];

    if ( is_user_logged_in() ) {
        $current_user = wp_get_current_user();
        write_log( 'Personal Message For '. $current_user->ID ) ;

        $wpdb->get_results('DELETE FROM wp_measure_images WHERE userid = '.$current_user->ID.' and id = '.$mid.'');
        

        $measureimages = $wpdb->get_results("SELECT * FROM wp_measure_images WHERE userid = $current_user->ID ");

        write_log($measureimages);

        
        $content = '<div id="mesureMentprintMe" class="product-plp-grid product-grid swatch "><div class="row product-row" id="aniimated-thumbnials">';

        $content .= '<h3 style="text-align: center; font-size: 33px;"><span>SAVED MEASUREMENTS</span></h3>';

        foreach($measureimages as $img){

           

            $img_path = str_replace('/var/www/html', '', $img->image_path);

            $content .= '<div class="col-lg-4 col-md-4 col-sm-6 "><div class="fl-post-grid-post">';
            $content .= '<div class="fl-post-grid-image prod_like_wrap mesureMentWrap">';

            $content .= '<a href="'.home_url().''.$img_path.'" title="#" class="wplightbox"><img class="measure_img" src="'.home_url().''.$img_path.'" /></a>';

            $content .= '<div class="favButtons button-wrapper">
                <a class="button fl-button" href="javascript:void(0)" title="#" onclick="openPopUp(this)"   data-img="'.home_url().''.$img_path.'" data-title="'.$img->image_name.'" onclik="openPopUp(e)">VIEW</a>';
                $content .= '<a class="button fl-button deletemeasure" href="#" data-id="'.$img->id.'"><i class="fa fa-trash" aria-hidden="true"></i></a>'; 
                $content .= '</div> </div>';
                $content .= '<h3><span>'.$img->image_name.'</span></h3>';

                $content .= "</div></div>";

        }

        $content .= "</div></div>";

        echo $content;
    }

  wp_die();

}

add_action( 'wp_ajax_nopriv_add_favroiute', 'add_favroiute' );
add_action( 'wp_ajax_add_favroiute', 'add_favroiute' );

function add_favroiute() {

    global $wpdb;

    if ( is_user_logged_in() ) {
        $current_user = wp_get_current_user();
        write_log( 'Personal Message For '. $current_user->ID ) ;
     
        $data=array(
            'user_id' => $current_user->ID, 
            'product_id' => $_POST['post_id']           
        );    
    
         $wpdb->insert( 'wp_favorite_posts', $data);        

         return true;

         wp_die();

    }else{

        return false;

        wp_die();
    }
}

add_action( 'wp_ajax_nopriv_remove_favroiute', 'remove_favroiute' );
add_action( 'wp_ajax_remove_favroiute', 'remove_favroiute' );

function remove_favroiute() {    

    global $wpdb;

    if ( is_user_logged_in() ) {
        $current_user = wp_get_current_user();
        write_log( 'Personal Message For '. $current_user->ID ) ;
     
        $data=array(
            'user_id' => $_POST['user_id'], 
            'product_id' => $_POST['post_id']           
        );    
    
        $wpdb->get_results('DELETE FROM wp_favorite_posts WHERE user_id = '.$_POST['user_id'].' and product_id = '.$_POST['post_id'].'');

         return true;

         wp_die();

    }else{

        return false;

        wp_die();
    }

}

// Favorite product shortcodes 
function greatcustom_favorite_posts_function(){

    global $wpdb;
    $content = "";
    $fav_sql = 'SELECT product_id FROM wp_favorite_posts WHERE user_id = '.get_current_user_id().'';
            
    $check_fav = $wpdb->get_results($fav_sql);    

    write_log($check_fav);

    $fav_products = array();

    foreach($check_fav as $fav){

        $fav_products[] = $fav->product_id;
    }

    write_log('dsfsd'.$fav_products);
    
    $favorite_post_ids = array_reverse($fav_products);
    $page = intval(get_query_var('paged'));

    $post_array = array('carpeting','hardwood_catalog','laminate_catalog','luxury_vinyl_tile','tile_catalog',
     'instock_laminate', 'instock_carpet', 'instock_hardwood', 'instock_luxury_vinyl', 'instock_tile');

        $brandmapping = array(
            "Carpet"=>"carpeting",
            "Hardwood"=>"hardwood_catalog",
            "Laminate"=>"laminate_catalog",
            "Luxury Vinyl Tile"=>"luxury_vinyl_tile",
            "Ceramic Tile"=>"tile_catalog",
            "Waterproof"=>"solid_wpc_waterproof",
            "Instock Carpet"=>"instock_carpet",
            "Instock Hardwood"=>"instock_hardwood",
            "Instock Laminate"=>"instock_laminate",
            "Instock Luxury Vinyl"=>"instock_luxury_vinyl",
            "Instock Tile"=>"instock_tile"
        );
    

   

    $content .= '<div id="ajaxreplace"><div class="favProWarpper printWrap">
                    <div class="shareAndPrint hideShare">
                        <a href="#"  id="share-button-3" class="need-share-button-default" data-share-position="middleBottom" data-share-networks="Mailto,Twitter,Pinterest,Facebook,GooglePlus,Linkedin"></a>
                        <a href="#" class="printDOc" ><i class="fa fa-print" aria-hidden="true"></i></a>
                    </div>
                <div class="row statiContent">
                    <div class="col-lg-12">
                        <h2>My Favorite Products </h2>
                        <p>Our product catalog is home to thousands of products. 
                        Collect your favorites and save them here to help you select the right product for your home.</p>
                    </div>
                </div>';
    
    if(!empty($check_fav)) {

        
        foreach($post_array as $posttype){

            $qry = array('post__in' => $favorite_post_ids, 'posts_per_page'=> $post_per_page, 'orderby' => 'post__in', 'paged' => $page);
            // custom post type support can easily be added with a line of code like below.
           //  $qry['post_type'] = array('carpeting','hardwood_catalog','laminate_catalog','luxury_vinyl_tile','tile_catalog');
           $qry['post_type'] = array($posttype);
           
           query_posts($qry);
    
           $post_type_name = array_search($posttype,$brandmapping);
            
           $content .= '<div id="printMe" class="product-plp-grid product-grid swatch "><div class="row product-row">';
            if ( have_posts() ) :
                $content .= '<h3>'.$post_type_name.'</h3>';
            endif;
            while ( have_posts() ) : the_post();

            $note_sql = 'SELECT note FROM wp_favorite_posts WHERE user_id = '.get_current_user_id().' and product_id='.get_the_ID().'';
            
            $check_note = $wpdb->get_results($note_sql, ARRAY_A);  
            // write_log('check_note');
            // write_log($check_note[0]['note']);
               
            $content .= '<div class="col-lg-4 col-md-4 col-sm-6 "><div class="fl-post-grid-post">';
            $content .= '<div class="fl-post-grid-image prod_like_wrap">';
    
                $image = swatch_image_product_thumbnail(get_the_ID(),'222','222');
    
                $sku = get_post_meta(get_the_ID(),'sku',true);
                $manufacturer = get_post_meta(get_the_ID(),'manufacturer',true);
    
                $content .= '<a href="'.get_permalink().'" title="'.get_the_title() .'"><img crossorigin="Anonymous"  class="list-pro-image" src="'. $image.'" alt="'.get_the_title().'" /></a>';
    
                $content .= '<div class="favButtons button-wrapper">
                    <a class="button fl-button" href="'.get_permalink().'" title="'.get_the_title() .'">VIEW PRODUCT</a>';
                   // $content .=   roomvo_script_integration($manufacturer,$sku,get_the_ID());
                   if($check_note[0]['note'] == '' || $check_note[0]['note'] == null){
                    $content .= '<a class="button fl-button add_note_fav" id="add_note_fun" href="javascript:void(0)" data-productid="'.get_the_ID().'">ADD A NOTE</a>'; 
                   }
                    $content .= '<div class="buttonHolder"><a id="rem_fav" data-id="'.get_the_ID().'" class="remove-parent" data-user_id="'.get_current_user_id().'" href="javascript:void(0)"><i class="fa fa-trash" aria-hidden="true"></i></a>';

                    if($check_note[0]['note'] != '' || $check_note[0]['note'] != null){
                        $content .= '<a class="view_note_fav" data-note="'.$check_note[0]['note'].'" id="view_note_fun" href="javascript:void(0)" data-productid="'.get_the_ID().'"><i class="fa fa-sticky-note" aria-hidden="true"></i></a>'; 
                        // $content .= '<div id="'.get_the_ID().'" style="display:none">'.$check_note[0]['note'].'</div>';
                       }
                    $content .= '</div></div> </div>';
                    $content .= ' <div class="fl-post-grid-text product-grid btn-grey">';
                    $content .= '<h4><span>'.get_the_title().'</span></h4>';
                    $content .= '</div>';
    
                //wpfp_remove_favorite_link(get_the_ID());
                $content .= "</div></div>";
            endwhile;
            $content .= "</div></div>";
        
    
        }
        echo '</div></div>';

    return $content;

 }


}
add_shortcode('greatcustom_favorite_posts', 'greatcustom_favorite_posts_function');


add_action( 'wp_ajax_nopriv_remove_favroiute_list', 'remove_greatcustom_favorite_posts_function' );
add_action( 'wp_ajax_remove_favroiute_list', 'remove_greatcustom_favorite_posts_function' );
function remove_greatcustom_favorite_posts_function(){

    global $wpdb;

    $content = "";

    $wpdb->get_results('DELETE FROM wp_favorite_posts WHERE product_id = '.$_POST['post_id'].' and user_id = '.get_current_user_id().'');    

    $fav_sql = 'SELECT product_id FROM wp_favorite_posts WHERE user_id = '.get_current_user_id().'';
            
    $check_fav = $wpdb->get_results($fav_sql);    

    write_log($check_fav);

    $fav_products = array();

    foreach($check_fav as $fav){

        $fav_products[] = $fav->product_id;
    }

    write_log('dsfsd'.$fav_products);
    
    $favorite_post_ids = array_reverse($fav_products);
    $page = intval(get_query_var('paged'));

    $post_array = array('carpeting','hardwood_catalog','laminate_catalog','luxury_vinyl_tile','tile_catalog');

        $brandmapping = array(
            "Carpet"=>"carpeting",
            "Hardwood"=>"hardwood_catalog",
            "Laminate"=>"laminate_catalog",
            "Luxury Vinyl Tile"=>"luxury_vinyl_tile",
            "Ceramic Tile"=>"tile_catalog",
            "Waterproof"=>"solid_wpc_waterproof"
        );
    

   

    $content .= '<div class="favProWarpper printWrap">
                    <div class="shareAndPrint hideShare">
                        <a href="#"  id="share-button-3" class="need-share-button-default" data-share-position="middleBottom" data-share-networks="Mailto,Twitter,Pinterest,Facebook,GooglePlus,Linkedin"></a>
                        <a href="#" class="printDOc" ><i class="fa fa-print" aria-hidden="true"></i></a>
                    </div>
                <div class="row statiContent">
                    <div class="col-lg-12">
                        <h2>My Favorite Products</h2>
                        <p>Our product catalog is home to thousands of products. 
                        Collect your favorites and save them here to help you select the right product for your home.</p>
                    </div>
                </div>';
    
    if(!empty($check_fav)) {

        
        foreach($post_array as $posttype){

            $qry = array('post__in' => $favorite_post_ids, 'posts_per_page'=> $post_per_page, 'orderby' => 'post__in', 'paged' => $page);
            // custom post type support can easily be added with a line of code like below.
           //  $qry['post_type'] = array('carpeting','hardwood_catalog','laminate_catalog','luxury_vinyl_tile','tile_catalog');
           $qry['post_type'] = array($posttype);
           
           query_posts($qry);
    
           $post_type_name = array_search($posttype,$brandmapping);
            
           $content .= '<div id="printMe" class="product-plp-grid product-grid swatch "><div class="row product-row">';
            if ( have_posts() ) :
                $content .= '<h3>'.$post_type_name.'</h3>';
            endif;
            while ( have_posts() ) : the_post();

            $note_sql = 'SELECT note FROM wp_favorite_posts WHERE user_id = '.get_current_user_id().' and product_id='.get_the_ID().'';
            
            $check_note = $wpdb->get_results($note_sql, ARRAY_A);  
            write_log('check_note');
            write_log($check_note[0]['note']);
               
            $content .= '<div class="col-lg-4 col-md-4 col-sm-6 "><div class="fl-post-grid-post">';
            $content .= '<div class="fl-post-grid-image prod_like_wrap">';
    
                $image = swatch_image_product_thumbnail(get_the_ID(),'222','222');
    
                $sku = get_post_meta(get_the_ID(),'sku',true);
                $manufacturer = get_post_meta(get_the_ID(),'manufacturer',true);
    
                $content .= '<a href="'.get_permalink().'" title="'.get_the_title() .'"><img class="list-pro-image" crossorigin="Anonymous"  src="'. $image.'" alt="'.get_the_title().'" /></a>';
    
                $content .= '<div class="favButtons button-wrapper">
                    <a class="button fl-button" href="'.get_permalink().'" title="'.get_the_title() .'">VIEW PRODUCT</a>';
                   // $content .=   roomvo_script_integration($manufacturer,$sku,get_the_ID());
                   if($check_note[0]['note'] == '' || $check_note[0]['note'] == null){
                    $content .= '<a class="button fl-button add_note_fav" id="add_note_fun" href="javascript:void(0)" data-productid="'.get_the_ID().'">ADD A NOTE</a>'; 
                   }
                    $content .= '<div class="buttonHolder"><a id="rem_fav" data-id="'.get_the_ID().'" class="remove-parent" data-user_id="'.get_current_user_id().'" href="javascript:void(0)"><i class="fa fa-trash" aria-hidden="true"></i></a>';

                    if($check_note[0]['note'] != '' || $check_note[0]['note'] != null){
                        $content .= '<a class="view_note_fav" data-note="'.$check_note[0]['note'].'" id="view_note_fun" href="javascript:void(0)" data-productid="'.get_the_ID().'"><i class="fa fa-sticky-note" aria-hidden="true"></i></a>'; 
                        $content .= '<div id="'.get_the_ID().'" style="display:none">'.$check_note[0]['note'].'</div>';
                       }
                    $content .= '</div></div> </div>';
                    $content .= ' <div class="fl-post-grid-text product-grid btn-grey">';
                    $content .= '<h4><span>'.get_the_title().'</span></h4>';
                    $content .= '</div>';
    
                //wpfp_remove_favorite_link(get_the_ID());
                $content .= "</div></div>";
            endwhile;
            $content .= "</div></div>";
        
    
        }

        $content .='</div></div>';
        

    echo $content;

    wp_die();

 }


}

add_action('wp_ajax_add_note_form', 'add_note_form');
add_action('wp_ajax_nopriv_add_note_form', 'add_note_form');

function add_note_form()
{
    global $wpdb;

    $fav_sql = 'UPDATE wp_favorite_posts SET note = "'.$_POST['note'].'" WHERE user_id = '.get_current_user_id().' and product_id = '.$_POST['addnote_productid'].'';
            
    $check_fav = $wpdb->get_results($fav_sql); 

    echo $fav_sql;
}

/*::::::::::::::::::::::::::::Workbook code end heree:::::::::::::::::::::::::::: */
function check_preferred_store(){
//var_dump($_COOKIE['preferred_store']);
}
add_action( 'init', 'check_preferred_store' );

add_filter( 'gform_pre_render_16', 'populate_location' );
add_filter( 'gform_pre_validation_16', 'populate_location' );
add_filter( 'gform_pre_submission_filter_16', 'populate_location' );
add_filter( 'gform_admin_pre_render_16', 'populate_location' );

add_filter( 'gform_pre_render_23', 'populate_location' );
add_filter( 'gform_pre_validation_23', 'populate_location' );
add_filter( 'gform_pre_submission_filter_23', 'populate_location' );
add_filter( 'gform_admin_pre_render_23', 'populate_location' );

add_filter( 'gform_pre_render_32', 'populate_location' );
add_filter( 'gform_pre_validation_32', 'populate_location' );
add_filter( 'gform_pre_submission_filter_32', 'populate_location' );
add_filter( 'gform_admin_pre_render_32', 'populate_location' );

add_filter( 'gform_pre_render_19', 'populate_location' );
add_filter( 'gform_pre_validation_19', 'populate_location' );
add_filter( 'gform_pre_submission_filter_19', 'populate_location' );
add_filter( 'gform_admin_pre_render_19', 'populate_location' );
function populate_location( $form ) {
 
    foreach ( $form['fields'] as &$field ) {
 
        if ( $field->type != 'select' || strpos( $field->cssClass, 'populate-location' ) === false ) {
            continue;
        }
 
        // you can add additional parameters here to alter the posts that are retrieved
        // more info: http://codex.wordpress.org/Template_Tags/get_posts
       // $posts = get_posts( 'numberposts=-1&post_status=publish' );
       $current_location_id = $_COOKIE['preferred_store'];
       $current_location_arr = array('1144584'=>'Arlington Heights, IL','1144585'=>'Madison, WI','1302101'=>'Waukesha, WI');
        
       $choices = array();
        $locations = $field->choices;
        foreach ( $locations as $location ) {
            $isselected = false;
            if($current_location_arr[$current_location_id] == $location['value']){
                $isselected = true;
            }
            $choices[] = array( 'text' => $location['text'], 'value' => $location['value'], 'isSelected'=> $isselected );
        }
 
        // update 'Select a Post' to whatever you'd like the instructive option to be
        $field->placeholder = 'Select a location';
        $field->choices = $choices;
 
    }
 
    return $form;
}

//add method to register event to WordPress init

add_action( 'init', 'register_daily_mysql_bin_log_event');
 
function register_daily_mysql_bin_log_event() {
    // make sure this event is not scheduled
    if( !wp_next_scheduled( 'mysql_bin_log_job' ) ) {
        // schedule an event
        wp_schedule_event( time(), 'daily', 'mysql_bin_log_job' );
    }
}

add_action( 'mysql_bin_log_job', 'mysql_bin_log_job_function' );
 

function mysql_bin_log_job_function() {
   
    global $wpdb;
    $yesterday = date('Y-m-d',strtotime("-1 days"));
    $sql_delete = "PURGE BINARY LOGS BEFORE '$yesterday'" ;						
	$delete_endpoint = $wpdb->get_results($sql_delete);
    write_log($sql_delete);	
}

add_filter( 'auto_update_plugin', '__return_false' );

/*
//Cron job for sync catalog for all mohawk categories
if (! wp_next_scheduled ( 'sync_mohawk_catalog_all_categories')) {
  
    wp_schedule_event( time(), 'each_monday', 'sync_mohawk_catalog_all_categories');
}
*/
//add_action( 'sync_mohawk_catalog_all_categories', 'mohawk_product_sync', 10, 2 );

function mohawk_product_sync(){

    write_log("Only mohawk Catalog sync is running"); 

    global $wpdb;
    $upload = wp_upload_dir();
    $upload_dir = $upload['basedir'];
    $upload_dir = $upload_dir . '/sfn-data';  
    
    $table_posts = $wpdb->prefix.'posts';
	$table_meta = $wpdb->prefix.'postmeta';	
    $product_json =  json_decode(get_option('product_json'));     

    $brandmapping = array(
        "hardwood"=>"hardwood_catalog",
        "laminate"=>"laminate_catalog",
        "lvt"=>"luxury_vinyl_tile",
        "tile"=>"tile_catalog"      
    );

    foreach($brandmapping as $key => $value){               
       
            $productcat_array = getArrayFiltered('productType',$key,$product_json);               

            
            foreach ($productcat_array as $procat){

                if($procat->manufacturer == "Mohawk"){

                    $permfile = $upload_dir.'/'.$value.'_'.$procat->manufacturer.'.json';
                    $res = SOURCEURL.get_option('SITE_CODE').'/www/'.$key.'/'.$procat->manufacturer.'.json?'.SFN_STATUS_PARAMETER;
                    $tmpfile = download_url( $res, $timeout = 900 );

                        if(is_file($tmpfile)){
                            copy( $tmpfile, $permfile );
                            unlink( $tmpfile ); 
                        } 

                          $sql_delete = "DELETE p, pm FROM $table_posts p INNER JOIN $table_meta pm ON pm.post_id = p.ID  WHERE p.post_type = '$value' AND pm.meta_key = 'manufacturer' AND  pm.meta_value = 'Mohawk'" ;						
                          write_log($sql_delete); 
                          $delete_endpoint = $wpdb->get_results($sql_delete);
                          write_log("mohawk product deleted"); 
                          //exit;

                        write_log('auto_sync - Shaw catalog - '.$key.'-'.$procat->manufacturer);
                       $obj = new Example_Background_Processing();
                       $obj->handle_all($value, $procat->manufacturer);

                        write_log('Sync Completed - '.$procat->manufacturer);

                  }
                    
                }
                
                   write_log('Sync Completed for all  '.$key.' brand');
        }                
}



function saleslideBgimages($arg){
    ob_start();
    $seleinformation = json_decode(get_option('salesliderinformation'));

    if(isset($seleinformation )){

    usort($seleinformation, 'compare_cde_some_objects');        
    }

    $website_json =  json_decode(get_option('website_json'));
    

    if(isset($seleinformation)){  
        
        $i = 0;
        foreach($seleinformation as $slide){
            $slider_bg_img = $slide->slider->slider_bg_img;
            
            
            if($i == 0){

if(preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo 
|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i" 
, $_SERVER["HTTP_USER_AGENT"])){
                echo '<link rel="preload" class="shortCUt" fetchpriority="high" href="https://mm-media-res.cloudinary.com/image/fetch/c_crop,h_768,w_768/'.$slider_bg_img.'" as="image" />';

    
            }else { 
                echo '<link rel="preload" class="shortCUt" fetchpriority="high" href="'.$slider_bg_img.'" as="image" />';
            }

            }
           
              $i++; 
          
        }
       
    }  
    return $link;

}
add_action('wp_head', 'saleslideBgimages', 0);