<?php get_header();
$rewrite_rule = maybe_unserialize(get_option("cptui_post_types"));

$rewrite_slug = $rewrite_rule["sheet_vinyl"]["rewrite_slug"];

$show_size_on_pdp = "no";
if (
	get_option("groupbysize") == "1" && is_array(get_option("groupbysize_cat"))	&& in_array("sheet", get_option("groupbysize_cat")) && get_option("groupbysizepdp") == "1"
) {
	$show_size_on_pdp = "yes";
}

wp_localize_script('wp-product-filter-react', 'wpProductCategory', array(
	'category' => "sheet",
	'sku' => get_the_content(),
	'rewrite_slug' => $rewrite_slug,
	'show_size_on_pdp' => $show_size_on_pdp,
)); ?>

<div class="container">
	<div class="row">
		<div class="fl-content product col-sm-12 ">
			<div id="mm-product-details"></div>
		</div>
		<?php //FLTheme::sidebar('right'); 
		?>
	</div>
</div>
<?php
$pdpshowform = get_option('pdpshowform') ? get_option('pdpshowform') : 0;
if ($pdpshowform == 1) {
	$form_shortcode =	get_option('pdpshowforminstock') ? get_option('pdpshowforminstock') : "";
	if ($form_shortcode != "") {
		$gravity_form_html = do_shortcode($form_shortcode);
?>
		<div id="gravity-form-root" data-gravity-form="<?php echo esc_attr($gravity_form_html); ?>"></div>
<?php
	}
}
?>
<?php get_footer(); ?>